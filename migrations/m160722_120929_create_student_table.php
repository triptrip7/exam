<?php

use yii\db\Migration;

/**
 * Handles the creation for table `student`.
 */
class m160722_120929_create_student_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('student', [
            'id' => 'pk',
			'name' => 'string',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('student');
    }
}
