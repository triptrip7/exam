<?php

use yii\db\Migration;

/**
 * Handles the creation for table `lead`.
 */
class m160723_080741_create_lead_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(
            'lead',
            [
                'id' => 'pk',
                'name' => 'string',	
				'email' => 'string',
				'phone' => 'string',	
                'notes' => 'text',
				'status' => 'integer',
				'owner' => 'integer',
				'created_at'=>'integer',
				'updated_at'=>'integer',
				'created_by'=>'integer',
				'updated_by'=>'integer'				
            ],
            'ENGINE=InnoDB'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('lead');
    }
}
