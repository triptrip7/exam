<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;


class LeadupdateController extends Controller
{
	public function actionAddrule()
	{
		$auth = Yii::$app->authManager;				
		$updateOwnLead = $auth->getPermission('updateOwnLead');
		
		$rule = new \app\rbac\OwnLeadRule;
		$auth->add($rule);
				
		$updateOwnLead->ruleName = $rule->name;		
		$auth->add($updateOwnLead);	
	}
}