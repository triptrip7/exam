<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Student;

class StudentController extends Controller
{
    public function actionView($id)
    {
        $model = Student::findOne($id);
		$name = $model->name;
		return $this->render('showOne' , ['name' => $name]);
    }
	
	public function actionAdd(){
		
		$student= new Student();
		$student->name = 'yoni';
		$student->save();
		
		$student= new Student();
		$student->name = 'david';
		$student->save();
		
		$student= new Student();
		$student->name = 'tal';
		$student->save();
		
		$student= new Student();
		$student->name = 'rotem';
		$student->save();
		
		return $this->goHome();
	}
}
