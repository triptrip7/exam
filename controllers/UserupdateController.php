<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;


class UserupdateController extends Controller
{
	public function actionAddrule()
	{
		$auth = Yii::$app->authManager;				
		$updateOwnUser = $auth->getPermission('updateOwnUser');
		
		$rule = new \app\rbac\OwnUserRule;
		$auth->add($rule);
				
		$updateOwnUser->ruleName = $rule->name;		
		$auth->add($updateOwnUser);	
	}
}