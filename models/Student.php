<?php

namespace app\models;
use \yii\db\ActiveRecord;

class Student extends ActiveRecord
{

    public static function tableName(){
		return 'student';
	}

	public function getName(){
		return $this->name;
	}

}