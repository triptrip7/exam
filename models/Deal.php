<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use app\models\Lead;

/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property integer $leadId
 * @property string $name
 * @property integer $amount
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['leadId', 'name', 'amount'], 'required'],
            [['leadId', 'amount'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadId' => 'Lead ID',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }
	
		public function getLeadItem()
    {
        return $this->hasOne(Lead::className(), ['id' => 'leadId']);
    }
}
