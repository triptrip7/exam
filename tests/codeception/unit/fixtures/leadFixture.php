<?php
namespace app\tests\codeception\unit\fixtures;

use yii\test\ActiveFixture;

class LeadFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Lead';
}