<?php

namespace tests\codeception\unit\models;

use Yii;
use yii\codeception\TestCase;
use Codeception\Specify;
use app\models\LoginForm;

class RbacTest extends TestCase
{
    use Specify;
	public $user1;
	public $user2;
	
    protected function setUp()
    {
		parent::setUp();
        $this->user1 = new LoginForm([
            'username' => 'admin',
            'password' => 'admin',
        ]);	
        $this->user2 = new LoginForm([
            'username' => 'user',
            'password' => 'user',
        ]);		
    }

	protected function tearDown()
    {
        Yii::$app->user->logout();
        parent::tearDown();
    }

    public function testAdmin()
    {
		
        $user1 = $this->user1;
		$user1->login();		
		
		$this->specify('Adnin can create User', function () use ($user1) {
            expect('Can() returns True for Admin', Yii::$app->user->can('createUser'))->true();
        });
		
		Yii::$app->user->logout();
		
		$user2 = $this->user2;
		$user2->login();
        
		$this->specify('Team leader cannot create User', function () use ($user2) {
            expect('Can() returns False for Team leader', Yii::$app->user->can('createUser'))->false();
        });		
		
    }

}