<?php

namespace tests\codeception\unit\models;

use Yii;
use yii\codeception\TestCase;
use app\models\Lead;
use app\tests\codeception\unit\fixtures\LeadFixture;
use Codeception\Specify;

class fixtureTest extends TestCase
{
    use Specify;

    public function fixtures()
    {
        return [
            'leads' => LeadFixture::className(),
        ];
    }
	
    public function testExistLead()
    {
		$model = Lead::find()
		->where(['email' => 'jack@jerde.com'])
		->one();

        $this->specify('Lead Should Exist in DB', function () use ($model) {
            expect('Name should be correct', $model->name=='Jack')->true();

        });
    }
		
	
}