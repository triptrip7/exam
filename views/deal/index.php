<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DealSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Deals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deal-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
		<?php if (\Yii::$app->user->can('createDeal') ){ ?>
        <?= Html::a('Create Deal', ['create'], ['class' => 'btn btn-success']) ?>
		<?php } ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
				'attribute' => 'leadId',
				'label' => 'Lead',
				'format' => 'raw',
				'value' => function($model){
					return $model->leadItem->name;
				},
				'filter'=>Html::dropDownList('DealSearch[leadId]', $lead, $leads, ['class'=>'form-control']),
			],
            'name',
            'amount',

            ['class' => 'yii\grid\ActionColumn',
			'visibleButtons' => [
			'delete' => (\Yii::$app->user->can('deleteDeal')),
			'update' => (\Yii::$app->user->can('deleteDeal')),
			],
			],
        ],
    ]); ?>
</div>
