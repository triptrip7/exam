<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
		<?php if (\Yii::$app->user->can('updateUser') || 
		    \Yii::$app->user->can('updateOwnUser', ['user' =>$model]) ){ ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?php } ?>
		
		<?php if (\Yii::$app->user->can('deleteUser')) { ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'password',
            //'auth_key',
            'firstname',
            'lastname',
            'email:email',
            'phone',
			[ // The role of the user
				'label' => $model->attributeLabels()['role'],
				'value' => $model->userole,
			],
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',
        ],
    ]) ?>

</div>
